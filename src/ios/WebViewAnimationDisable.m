#import "WebViewAnimationDisable.h"

@implementation WebViewAnimationDisable

- (void) pluginInitialize
{
	[UIView setAnimationsEnabled:NO];
}

@end
